package storm.starter;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.ShellBolt;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import storm.starter.spout.RandomSentenceSpout;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * This topology demonstrates Storm's stream groupings and multilang
 * capabilities.
 */
public class FancyTopology {
	public static class FancyData {
		public static interface FancyDataMap {
			public int map(int cval);
		}

		protected int[][] randArray;

		public FancyData(int size, int defval) {
			randArray = new int[size][size];
			for (int i = 0; i < randArray.length; i++)
				for (int j = 0; j < randArray[0].length; j++)
					randArray[i][j] = defval;

		}

		public void applyOp(FancyDataMap curOp) {
			for (int i = 0; i < randArray.length; i++) {
				for (int j = 0; j < randArray[0].length; j++) {
					randArray[i][j] = curOp.map(randArray[i][j]);
				}
			}
		}
		@Override
		public String toString() {
			double v=0.0;
			int x=randArray.length;
			int y=randArray[0].length;
			for (int i = 0; i < x; i++) 
				for (int j = 0; j < y; j++) v+=randArray[i][j];
			return "fancyData:(mean="+v/(x*y)+")";
		}

	}

	public static class FancySpout extends BaseRichSpout {
		SpoutOutputCollector _collector;
		Random _rand;

		@Override
		public void open(Map conf, TopologyContext context,
				SpoutOutputCollector collector) {
			_collector = collector;
			_rand = new Random();
		}

		@Override
		public void nextTuple() {
			Utils.sleep(100);
			FancyData curData = new FancyData(100, _rand.nextInt(100));
			_collector.emit(new Values(curData));
		}

		@Override
		public void ack(Object id) {
		}

		@Override
		public void fail(Object id) {
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("image"));
		}

	}

	public static class AddValue extends BaseBasicBolt {
		Map<String, Integer> counts = new HashMap<String, Integer>();

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {
			FancyData curImg = (FancyData) tuple.getValue(0);
			curImg.applyOp(new FancyData.FancyDataMap() {
				public int map(int cval) {
					return cval + 1;
				}
			});

			collector.emit(new Values(curImg));
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("image"));
		}
	}
	
	public static class Histogram extends BaseBasicBolt {
		public static class Histo implements Serializable {
			Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
			public void bump(int cval) {
				Integer nval = new Integer(cval);
				Integer count = counts.get(nval);
				if (count == null)
					count = 0;
				count++;
				counts.put(nval, count);
			}
			@Override
			public String toString() {
				String outStr="hist:[";
				for (Integer cKey: counts.keySet()) {
					outStr+="("+cKey+","+counts.get(cKey)+"),";
				}
				return outStr+"]";
			}
		}
		protected Histogram.Histo cHist=new Histogram.Histo();

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {
			FancyData curImg = (FancyData) tuple.getValue(0);
			curImg.applyOp(new FancyData.FancyDataMap() {
				public int map(int cval) {
					cHist.bump(cval);
					return cval;
				}
			});

			collector.emit(new Values(cHist));
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("histogram"));
		}
	}

	public static void main(String[] args) throws Exception {

		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("spout", new FancySpout(), 5);

		builder.setBolt("add", new AddValue(), 8).shuffleGrouping("spout");
		
		builder.setBolt("hist", new Histogram(), 9).shuffleGrouping("add");

		Config conf = new Config();
		conf.setDebug(true);

		if (args != null && args.length > 0) {
			conf.setNumWorkers(3);

			StormSubmitter.submitTopology(args[0], conf,
					builder.createTopology());
		} else {
			conf.setMaxTaskParallelism(3);

			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("word-count", conf, builder.createTopology());

			Thread.sleep(10000);

			cluster.shutdown();
		}
	}
}
